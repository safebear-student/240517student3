package com.safebear.app;

import org.junit.Test;

import static org.junit.Assert.assertTrue;

/**
 * Created by Admin on 24/05/2017.
 */
public class Test01_Login extends BaseTest {
    @Test
    public void testLogin(){
        assertTrue(welcomePage.checkCorrectPage());
    //step 1 confirm we're on te Welcome Page

     //step 2 click on the Login link and the Login Page loads
        assertTrue(welcomePage.clickOnLogin(this.loginPage));

     //step 3 login in wit valid credentials
      assertTrue(loginPage.login(this.userPage,"testuser","testing"));
    }
}
