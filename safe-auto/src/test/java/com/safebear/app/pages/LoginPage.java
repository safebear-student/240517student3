package com.safebear.app.pages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Admin on 24/05/2017.
 */
public class LoginPage {
    WebDriver driver;
    @FindBy(id = "myid")
    WebElement usename_field;

    @FindBy(id = "mypass")
    WebElement password_field;

    public LoginPage(WebDriver driver) {
        this.driver = driver;
        PageFactory.initElements(driver, this);

    }

    public boolean login(UserPage userpage, String username, String password){
        this.usename_field.sendKeys(username);
        this.password_field.sendKeys(password);
        this.password_field.submit();
        return userpage.checkCorrectPage();
    }


    public boolean checkCorrectPage() {
        return driver.getTitle().startsWith("Sign");
    }
}
